import React from "react";
import "./footer.scss";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const Footer = () => {
  return (
    <div className="footer px-4 pt-4">
      <Container fluid >
        <Row>
          <Col xs={5}>
            <div>
              <h4>CONCEPT</h4>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Repellat minima voluptas exercitationem neque esse minima voluptas exercitationem neque esse minima voluptas exercitationem neque esse 
              </p>
            </div>
          </Col>
          <Col >
            <div className="mx-auto"> 
              <h6>Product</h6>
              <ul className="list-unstyled">
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
              </ul>
            </div>
          </Col>
          <Col>
            <div>
              <h6>Useful Line</h6>
              <ul className="list-unstyled">
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
              </ul>
            </div>
          </Col>
          <Col>
            <div>
              <h6>Address</h6>
              <ul className="list-unstyled">
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
                <li>Loewm ipsum</li>
              </ul>
            </div>
          </Col>
        </Row>
        <Row className="mt-3">
            <p className="text-center">coptright@2022</p>
        </Row>
      </Container>
    </div>
  );
};
export default Footer;
