import React from "react";
import "./body.scss";
import Carousel from "react-bootstrap/Carousel";
import Person from "../Componants/card";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import img1 from '../Images/1.png'
import img2 from '../Images/2.png'
import img3 from '../Images/3.png'


const Body = (props) => {
  return (
    <div className="body">
      <section className="hero">
        <Carousel>
          <Carousel.Item interval={600}>
            <img
              className="d-block w-100"
              src={require("../Images/1.png")}
              alt="First slide"
            />
            <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item interval={600}>
            <img
              className="d-block w-100"
              src={require("../Images/2.png")}
              alt="Second slide"
            />
            <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item interval={600}>
            <img
              className="d-block w-100"
              src={require("../Images/3.png")}
              alt="Third slide"
            />
            <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>
                Praesent commodo cursus magna, vel scelerisque nisl consectetur.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </section>
      <section className="card py-5">
        <Container fluid>
          <Row xs={3} gap={3}>
            <Col>
              <Person xyz={img1}></Person>
            </Col>
            <Col>
              <Person xyz={img2}></Person>
            </Col>
            <Col>
              <Person xyz={img3}></Person>
            </Col>
          </Row>
        </Container>
      </section>
      <section className="hero"></section>
    </div>
  );
};
export default Body;
