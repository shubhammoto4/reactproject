import Body from './Layout/Body';
import Footer from './Layout/Footer';
import Header from './Layout/Header';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  const carInfo = { name: "Ford", model: "Mustang" };
  return (
    <div className="App">
      <Header ></Header>
      <Body name="XYZ" address="abc" car={carInfo}></Body>
      <Footer></Footer>
    </div>
  );
}

export default App;
